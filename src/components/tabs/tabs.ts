import { Component } from '@angular/core';
import {Login} from '../../providers/login';
import {Home} from '../../pages/home/home';
import {Nav} from 'ionic-angular';
import {MessagesPage} from "../../pages/messages/messages";
import {ProfilePage} from "../../pages/profile/profile";
@Component({
  selector: 'tabs-app',
  templateUrl: 'tabs.html'
})
export class TabsComponent {

  tab1:{page:any,icon:string,title:string};
  tab2:{page:any,icon:string,title:string};
  tab3:{page:any,icon:string,title:string};
  tab4:{page:any,icon:string,title:string};
  cantNotifications:number;
  constructor(public login:Login,public nav:Nav) {
    this.tab1 = {page:Home,icon:"home",title:"Inicio"};
    this.tab2 = {page:MessagesPage,icon:"paper-plane",title:"Notificaciones"};
    this.tab3 = {page:ProfilePage,icon:"person",title:"Perfil"};
    if(localStorage.getItem('notifications')){
      let datos:any[] = JSON.parse(localStorage.getItem('notifications'));
      if(localStorage.getItem('messages')){
        let datos2:any[] = JSON.parse(localStorage.getItem('messages'));
        this.cantNotifications = datos.length + datos2.length;
      }else{
        this.cantNotifications = datos.length;
      }
    }else{
      this.cantNotifications = 0;
    }
  }

  isLogin():boolean{
    return this.login.isLogin();
  }

}
