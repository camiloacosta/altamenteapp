import { Component } from '@angular/core';
import { LoginPage } from '../../pages/login/login';
import { Login } from '../../providers/login';
import { ProfilePage } from '../../pages/profile/profile';
import { NavController, Nav } from 'ionic-angular';
@Component({
  selector: 'headernav',
  templateUrl: 'headernav.html'
})
export class HeadernavComponent {
  constructor(public navCtrl: NavController, private nav : Nav, public login:Login) {
  }

  goToProfileOrLogin(){
    if(this.login.isLogin()){
        this.navCtrl.push(ProfilePage);
    }else{
        this.navCtrl.push(LoginPage);
    }
  }

  isLogin():boolean{
    return this.login.isLogin();
  }

}
