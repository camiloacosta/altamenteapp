import { Component } from '@angular/core';

/*
  Generated class for the Pay component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'pay',
  templateUrl: 'pay.html'
})
export class PayComponent {

  text: string;

  constructor() {
    console.log('Hello Pay Component');
    this.text = 'Hello World';
  }

}
