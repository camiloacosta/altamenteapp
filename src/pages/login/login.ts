import { Component } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';
import {Http } from "@angular/http";
import {Login} from "../../providers/login";
import {MyApp} from "../../app/app.component";
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  private user:string;
  private pass:string;
  private error:string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http:Http,
    public login:Login,
    public nav:Nav) {
  }

  startLogin(){
    this.login.login(this.user,this.pass).subscribe(result=>{
      if(result === true){
        this.nav.setRoot(MyApp);
      }else{
        this.error = "Usuario o contraseña incorrecta";
      }
    })    
  }

}
