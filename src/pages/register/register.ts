import { Component } from '@angular/core';
import { NavController, NavParams, Nav,AlertController} from 'ionic-angular';
import { Register } from '../../providers/register';
import {MyApp} from "../../app/app.component";

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

    public correo_electronico:string;
    public nombres:string;
    public apellidos:string;
    public password:string;
    public password_repeat:string;
    public genero:string;
    public error:string=null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public register: Register,
    public nav:Nav,
    public alertCtrl: AlertController) {}

  
  goRegister(){
    this.register.register(
      this.correo_electronico,
      this.nombres,
      this.apellidos,
      this.password,
      this.password_repeat,
      this.genero
    ).subscribe(result=>{
      if(result === true){
        this.nav.setRoot(MyApp);
        let alert = this.alertCtrl.create({
          title: '¡Binvenido'+this.nombres+'!',
          subTitle: 'Puedes utilizar el correo y la contraseña que escogiste para ingresar a tu perfil.',
          buttons: ['OK']
        });
        alert.present();
      }else{
        this.error = "Error en registro intentalo nuevamente."
      }
    });
  }

}
