import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import * as simplewebrtc from 'simplewebrtc';
import {Login} from '../../providers/login';
import {AudioProvider} from 'ionic-audio';
@Component({
  selector: 'page-streaming',
  templateUrl: 'streaming.html'
})
export class StreamingPage {
  public room:string = 'papaya';
  public hasConference:boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public login:Login,
    public alertCtrl:AlertController) {
    if(navParams.get('room_id')){
      this.hasConference = true;
      this.room = navParams.get('room_id');
      this.iniciar();
    }
  }

  
  ionViewDidLoad() {
    let pay = this.login.pay;
    if(!this.login.pay){
      let confirm = this.alertCtrl.create({
      title: 'Aviso!',
      message: 'Para poder utilizar esta función de nuestra app, debes ser usuario Premium,estar inscrito a una maestría o certificación.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ser Premium',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
      confirm.present();
    }
  }

  iniciar() {
    var webrtc = new simplewebrtc({
      remoteVideosEl: 'remoteVideos',
    });
    webrtc.joinRoom(this.room);
    webrtc.on('videoAdded', (video,peer)=>{
      console.log(video,peer);
    });
    webrtc.on('createdPeer',(peer)=>{
      console.log(peer);
    });
  }

}
