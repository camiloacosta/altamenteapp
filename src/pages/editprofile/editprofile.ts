import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import {Http,Headers} from "@angular/http";
import {Login} from "../../providers/login";
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html'
})
export class EditprofilePage {

  profile:{
      usuario_id:string,
      username:string,
      correo_electronico:string,
      auth_key:string,
      last_connection:string,
      configuraciones:string,
      nombres:string,
      apellidos:string,
      foto:string,
      direccion:string,
      telefono:number,
      celular:number,
      genero:string,
      fecha_nacimiento:string,
      descripcion:string
  }={
      usuario_id:null,
      username:null,
      correo_electronico:null,
      auth_key:null,
      last_connection:null,
      configuraciones:null,
      nombres:null,
      apellidos:null,
      foto:null,
      direccion:null,
      telefono:null,
      celular:null,
      genero:null,
      fecha_nacimiento:null,
      descripcion:null
    };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public login:Login,
    public alertCtrl:AlertController) {}

  ionViewDidLoad() {
    this.loadProfile();
  }

  loadProfile(){
    let url = "http://secure.altamenteapp.com/api/user/profile?auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        this.profile=data;
      },
      error => console.error(error)
    );
  }

  changeDataProfile(){
    let url = "http://secure.altamenteapp.com/api/user/profile?auth-key="+this.login.token;
    let body = `Usuarios[correo_electronico]=${this.profile.correo_electronico}&Usuarios[nombres]=${this.profile.nombres}&Usuarios[apellidos]=${this.profile.apellidos}&Usuarios[direccion]=${this.profile.direccion}&Usuarios[telefono]=${this.profile.telefono}&Usuarios[celular]=${this.profile.celular}&Usuarios[genero]=${this.profile.genero}`;
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    this.http.post(url,body,{headers:headers}).subscribe(response=>{
      let data = response.json();
      if(data.error===false){
        this.loadProfile();
        let alert = this.alertCtrl.create({
          title: 'Cambios Guardados',
          subTitle: 'Se han guardado los cambios de manera exitosa',
          buttons: ['OK']
        });
        alert.present();
      }else{
        console.log("error");
      }
    });
  }

}
