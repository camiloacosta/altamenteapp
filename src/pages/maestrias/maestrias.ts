import { Component } from '@angular/core';

import {NavController, NavParams,Nav,AlertController} from 'ionic-angular';
import { SuscribePage } from '../suscribe/suscribe';
@Component({
  selector: 'page-maestrias',
  templateUrl: 'maestrias.html'
})
export class Maestrias {

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController, public nav:Nav) {

  }


  goToSuscribe(id,name){
    this.navCtrl.push(SuscribePage,{id:id, name:name});
  }
}
