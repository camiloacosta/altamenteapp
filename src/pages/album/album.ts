import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Login} from '../../providers/login';
import {Http} from '@angular/http';

@Component({
  selector: 'page-album',
  templateUrl: 'album.html'
})
export class AlbumPage {
  
  public idAlbum:string=null;

  public albums:Array<{
    album_id:string,
    nombre_archivo:string,
    fecha:string,
    foto_id:string,
    url:string
  }>=[];

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   public login:Login,
   public http:Http) {
    this.idAlbum = navParams.get('id');
  }

  ionViewDidLoad() {
    this.loadAlbums();
  }

  loadAlbums(){
    let url = "http://secure.altamenteapp.com/api/courses/album?id="+this.idAlbum+"&auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json().fotos;
        for(let i=0;i<data.length;i++){
          let datos = data[i];
          this.albums.push(datos);
        }
      },
      error => console.error(error)
    );
  }

}
