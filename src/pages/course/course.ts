import { Component } from '@angular/core';
import { NavController, NavParams, Nav} from 'ionic-angular';
import {AudioPage} from '../audio/audio';
import {PhotogalleryPage} from '../photogallery/photogallery';
import {EventsPage} from '../events/events';
import {SurveysPage} from '../surveys/surveys';
@Component({
  selector: 'page-course',
  templateUrl: 'course.html'
})
export class CoursePage {

  public courseData:{
    curso_id: string,
    estado: string,
    fecha_fin:string,
    fecha_inicio:string,
    nombre: string,
    tipo_curso:string
  }=null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public nav:Nav) {
    this.courseData = navParams.data;
  }

  goToMeditations(id:string){
    this.navCtrl.push(AudioPage,{id : id});
  }
  
  goToGallery(id:string){
    this.navCtrl.push(PhotogalleryPage,{id:id});
  }

  goToEvents(id:string){
    this.navCtrl.push(EventsPage,{id:id});
  }

  goToSurveys(id:string){
    this.navCtrl.push(SurveysPage,{id:id});
  }

}
