import { Component } from '@angular/core';

import { NavController, Nav } from 'ionic-angular';
import { Maestrias } from '../maestrias/maestrias';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {

  constructor(public navCtrl: NavController, private nav : Nav) {
    
  }

  goToMaestrias(){
    this.navCtrl.push(Maestrias);
  }


}
