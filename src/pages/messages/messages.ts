import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { StreamingPage } from '../streaming/streaming';
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage {
  streamings:Array<{type:string,body:string,room_id:string}>=[];
  messages:Array<{type:string,body:string}>=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl:AlertController) {
    this.loadNotifications();
  }

  ionViewDidLoad() {
    this.loadNotifications();
  }

  loadNotifications(){
    let notifications = JSON.parse(localStorage.getItem('notifications'));
    let messages = JSON.parse(localStorage.getItem('messages'));
    if(notifications){
      this.streamings = notifications;
    }

    if(messages){
      this.messages = messages;
    }
  }

  goToStreaming(room_id:string){
    this.navCtrl.push(StreamingPage,{room_id:room_id});
  }

  goToReadMessage(index:number){
    let alert = this.alertCtrl.create({
      title: this.messages[index].type,
      subTitle: this.messages[index].body,
      buttons: ['OK']
    });
    alert.present();
  }

  deleteNotify(index:number){
    this.streamings.splice(index,1);
    localStorage.setItem('notifications',JSON.stringify(this.streamings));
    this.loadNotifications();
  }

  deleteMessage(index:number){
    this.messages.splice(index,1);
    localStorage.setItem('messages',JSON.stringify(this.messages));
    this.loadNotifications();
  }
}
