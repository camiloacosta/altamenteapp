import { Component } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';
import {Login} from '../../providers/login';
import {Http} from '@angular/http';
import {PollsPage} from '../polls/polls';
@Component({
  selector: 'page-surveys',
  templateUrl: 'surveys.html'
})
export class SurveysPage {

  private idCourse;

  surveys:Array<{
    titulo:string,
    descripcion:string,
    fecha_inicio:string,
    fecha_fin:string,
    url:string
  }>=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public login:Login,
    public http:Http,
    public nav:Nav
    ) {
      this.idCourse = navParams.get('id');
    }

  ionViewDidLoad() {
    this.loadSurveys();
  }

  loadSurveys(){
    let url = "http://secure.altamenteapp.com/api/courses/surveys?id="+this.idCourse+"&auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for (let i=0;i<data.encuestas.length;i++){
          let datos = data.encuestas[i];
          this.surveys.push(datos);
        }
      },
      error => console.error(error)
    );
  }

  goToPoll(url:string){
    this.navCtrl.push(PollsPage,{url:url});    
  }

}
