import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {RegisterPage} from '../register/register';
@Component({
  selector: 'page-home-login',
  templateUrl: 'home-login.html'
})
export class HomeLoginPage {
  tab1={page:LoginPage,title:"Entrar"};
  tab2={page:RegisterPage,title:"Registrar"};
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeLoginPage');
  }

}
