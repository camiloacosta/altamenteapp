import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import {Login} from '../../providers/login';
import {AudioProvider} from 'ionic-audio';
@Component({
  selector: 'page-audio',
  templateUrl: 'audio.html'
})
export class AudioPage {
  public idCourse:string = null;
  
  myTracks: any[]=[];
  allTracks: any[]=[];
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http:Http,
    public login:Login,
    private _audioProvider: AudioProvider) {
      this.idCourse = navParams.get('id');
  }

  ngAfterContentInit() {     
    this.allTracks = this._audioProvider.tracks; 
  }
  
  playSelectedTrack() {
    this._audioProvider.play(this.myTracks[0]);
  }
  
  pauseSelectedTrack() {
     this._audioProvider.pause(this.myTracks[0]);
  }
         
  onTrackFinished(track: any) {
    console.log('Track finished', track)
  } 

  ionViewDidLoad() {
    this.loadAudios();
  }

  loadAudios(){
    let url = "http://secure.altamenteapp.com/api/courses/meditations?id="+this.idCourse+"&auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for (let i=0;i<data.length;i++){
          let datos = data[i];
          this.myTracks.push({
            src: "http://secure.altamenteapp.com"+datos.url,
            artist: datos.nombre_archivo,
            title: datos.nombre,
            art: "http://na.lvlt.sims3store.cdn.ea.com/u/f/sims/sims3/sims3store/mygoodies/avatarbackgrounds_free_soliddarkblue/Thumbnail_300x300.png",
            preload: 'true'
          });
        }
      },
      error => console.error(error)
    );
  }



}
