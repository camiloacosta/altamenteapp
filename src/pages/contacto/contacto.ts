import { Component } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import {Message} from '../../providers/message';
@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html'
})
export class ContactoPage {

  msj:{
    nombre:string,
    asunto:string,
    correo:string,
    celular:number,
    mensaje:string
  }={nombre:null,
    asunto:"Mensaje Contacto",
    correo:null,
    celular:null,
    mensaje:null};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public message:Message,
    public toastCtrl:ToastController) {}

  send(){
    this.message.send(this.msj).subscribe(response=>{
      if(response){
          let toast = this.toastCtrl.create({
            message: "Error al enviar, intentelo de nuevo",
            position: 'middle',
            duration: 3000
          });
          toast.present();
      }else{
          let toast = this.toastCtrl.create({
            message: "Mensaje enviado correctamente",
            position: 'middle',
            duration: 3000
          });
          toast.present();
      }
    });
  }
}
