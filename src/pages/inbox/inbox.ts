import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';

@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html'
})
export class InboxPage {
  messages:Array<{type:string,body:string}>=[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl:AlertController) {}

  ionViewDidLoad() {
    this.loadMessages();
  }

  loadMessages(){
    let messages = JSON.parse(localStorage.getItem('messages'));
    if(messages){
      this.messages = messages;
    }
  }

  goToReadMessage(index:number){
    let alert = this.alertCtrl.create({
      title: this.messages[index].type,
      subTitle: this.messages[index].body,
      buttons: ['OK']
    });
    alert.present();
  }

  deleteMessage(index:number){
    this.messages.splice(index,1);
    localStorage.setItem('messages',JSON.stringify(this.messages));
    this.loadMessages();
  }

}
