import { Component } from '@angular/core';
import { NavController, NavParams , Nav} from 'ionic-angular';
import {Http} from '@angular/http';
import {Login} from '../../providers/login';
/*
  Generated class for the Events page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {

  public idCourse:string = null;

  events:Array<{
    calendario_id:string,
    titulo:string,
    descripcion:string,
    fecha_hora_inicio:Date,
    fecha_hora_fin:Date
  }> = []

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public nav:Nav,
    public login:Login,
    public http:Http
    ) {
      this.idCourse = navParams.get("id");
  }

  ionViewDidLoad() {
    this.loadEvents();
  }

  loadEvents(){
    let url = "http://secure.altamenteapp.com/api/courses/calendar?auth-key="+this.login.token+"&id="+this.idCourse;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for (let i=0;i<data.events.length;i++){
          let datos = {
            calendario_id : data.events[i].calendario_id,
            titulo: data.events[i].titulo,
            descripcion:data.events[i].descripcion,
            fecha_hora_inicio:new Date(data.events[i].fecha_hora_inicio),
            fecha_hora_fin:new Date(data.events[i].fecha_hora_fin),
          }
          this.events.push(datos);
        }
      },
      error => console.error(error)
    );
  }

  isMonth(month:number):string{
    let mes = "";
    switch (month) {
      case 1:
        mes = "Ene"
        break;
      case 2:
        mes = "Feb"
        break;
      case 3:
        mes = "Mar"
        break;
      case 4:
        mes = "Abr"
        break;
      case 5:
        mes = "May"
        break;
      case 6:
        mes = "Jun"
        break;
      case 7:
        mes = "Jul"
        break;
      case 8:
        mes = "Ago"
        break;
      case 9:
        mes = "Sep"
        break;
      case 10:
        mes = "Oct"
        break;
      case 11:
        mes = "Nov"
        break;
      case 12:
        mes = "Dic"
        break;
    }
    return mes;
  }

}
