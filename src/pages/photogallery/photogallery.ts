import { Component } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';
import {Http} from '@angular/http';
import {Login} from '../../providers/login';
import {AlbumPage} from '../album/album';
/*
  Generated class for the Photogallery page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-photogallery',
  templateUrl: 'photogallery.html'
})
export class PhotogalleryPage {
  
  public idCourse:string=null;

  albums:Array<{
    album_id:string,
    curso_id:string,
    nombre:string,
    fecha:string,
    portada:string,
    numero_fotos:number
  }>=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http:Http,
    public login:Login,
    public nav: Nav) {
      this.idCourse = navParams.get('id'); 
    }

  ionViewDidLoad() {
    this.loadAlbums();
  }

  goToAlbum(id:string){
    this.nav.push(AlbumPage,{id:id});
  }

  loadAlbums(){
    let url = "http://secure.altamenteapp.com/api/courses/gallery?id="+this.idCourse+"&auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for(let i=0;i<data.length;i++){
          let datos = data[i];
          this.albums.push(datos);
        }
      },
      error => console.error(error)
    );
  }
}
