import { Component } from '@angular/core';
import { NavController, NavParams, Nav} from 'ionic-angular';
import {Login} from '../../providers/login';
import {MyApp} from "../../app/app.component";
import {Http,Headers} from '@angular/http';
import {CoursePage} from '../course/course';
import {AlertController,ToastController} from 'ionic-angular';
import {EditprofilePage} from '../editprofile/editprofile';
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  public foto:string;
  public nombres:string;
  public apellidos:string;
  public email:string;

  certificaciones: Array<{
      curso_id: string,
      estado: string,
      fecha_fin:string,
      fecha_inicio:string,
      nombre: string,
      tipo_curso:string
    }>=[];
  
  maestrias:Array<{
      curso_id: string,
      estado: string,
      fecha_fin:string,
      fecha_inicio:string,
      nombre: string,
      tipo_curso:string
    }>=[];
  
  
  constructor(
    public http:Http,
    public navCtrl: NavController,
    public navParams: NavParams,
    public login:Login,
    public nav:Nav,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController) {
    
  }

  ionViewDidLoad() {
    this.loadProfile()
    this.loadMyCourses();
  }

  Logout(){
    this.login.logout();
    this.nav.setRoot(MyApp);
  }


  loadProfile(){
    let url = "http://secure.altamenteapp.com/api/user/profile?auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        console.log(data);
        this.foto = "http://secure.altamenteapp.com"+data.foto;
        this.nombres = data.nombres;
        this.apellidos = data.apellidos;
        this.email = data.correo_electronico;
      },
      error => console.error(error)
    );
  }

    goToCourse(course : {
    curso_id: string,
    estado: string,
    fecha_fin:string,
    fecha_inicio:string,
    nombre: string,
    tipo_curso:string
  }){
    this.navCtrl.push(CoursePage,course);
  }

  loadMyCourses(){
    let url = "http://secure.altamenteapp.com/api/user/courses?auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for (let i=0;i<data.cursos.length;i++){
          let datos = data.cursos[i];
          if(datos.tipo_curso == "M"){
            this.maestrias.push(datos);
          }else{
            this.certificaciones.push(datos);
          }
        }
      },
      error => console.error(error)
    );
  }

  addCourse(){
    let prompt = this.alertCtrl.create({
          title: 'Tienes un pin?',
          message: "Porfavor digita el pin que te ha sido asignado, respetando letras mayusculas y luego oprime el boton Enviar.",
          inputs: [
            {
              name: 'pin',
              placeholder: 'Digite su Pin'
            },
          ],
          buttons: [
            {
              text: 'Cancelar',
              handler: data => {
                
              }
            },
            {
              text: 'Enviar',
              handler: data => {
                this.addCourseByPin(data.pin);
              }
            }
          ]
        });
        prompt.present();
  }

  addCourseByPin(pin:string){
    let url = "http://secure.altamenteapp.com/api/courses/register?auth-key="+this.login.token;
    let body = `CursosCodigos[codigo]=${pin}`;
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    this.http.post(url,body,{headers:headers}).subscribe(
      response=>{
        let data = response.json();
        if(data.error === false){
          this.loadMyCourses();
          localStorage.setItem('pay', JSON.stringify({pay:true})); 
        }else{      
          let toast = this.toastCtrl.create({
            message: data.message,
            position: 'middle',
            duration: 3000
          });
          toast.present();
        }
      }
    );
  }

  goToEditProfile(){
    this.navCtrl.push(EditprofilePage);
  }



}
