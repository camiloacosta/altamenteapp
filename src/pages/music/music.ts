import { Component } from '@angular/core';
import { NavController, NavParams,AlertController,Nav} from 'ionic-angular';
import {Login} from '../../providers/login';
import { MyApp } from '../../app/app.component';
import { PayPremiumPage } from '../pay-premium/pay-premium';
@Component({
  selector: 'page-music',
  templateUrl: 'music.html'
})
export class MusicPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public login:Login,
    public alertCtrl:AlertController,
    public nav:Nav) {}

    ionViewDidLoad() {
      if(!this.login.pay){
        let confirm = this.alertCtrl.create({
        title: 'Aviso!',
        message: 'Para poder utilizar esta función de nuestra app, debes ser usuario Premium,estar inscrito a una maestría o certificación.',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              this.nav.setRoot(MyApp);
            }
          },
          {
            text: 'Ser Premium',
            handler: () => {
              this.nav.setRoot(PayPremiumPage);
            }
          }
        ]
      });
        confirm.present();
    }
  }
}
