import { Component} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'page-polls',
  templateUrl: 'polls.html'
})
export class PollsPage {
  private url; 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sanitizer: DomSanitizer) {
    this.url = sanitizer.bypassSecurityTrustResourceUrl(navParams.get('url'));
  }
}
