import { Component } from '@angular/core';
import { NavController, NavParams,AlertController,Nav } from 'ionic-angular';
import {Http} from '@angular/http';
import {Login} from '../../providers/login';
import {AudioProvider} from 'ionic-audio';
import { MyApp } from '../../app/app.component';
import { PayPremiumPage } from '../pay-premium/pay-premium';
/*
  Generated class for the Meditaciones page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-meditaciones',
  templateUrl: 'meditaciones.html'
})
export class MeditacionesPage {
  myTracks: any[]=[];
  allTracks: any[]=[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http:Http,
    public login:Login,
    private _audioProvider: AudioProvider,
    public alertCtrl:AlertController,
    public nav:Nav) {}

  ionViewDidLoad() {
    this.loadAudios();

      if(!this.login.pay){
        let confirm = this.alertCtrl.create({
        title: 'Aviso!',
        message: 'Para poder utilizar esta función de nuestra app, debes ser usuario Premium,estar inscrito a una maestría o certificación.',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              this.nav.setRoot(MyApp);
            }
          },
          {
            text: 'Ser Premium',
            handler: () => {
              this.nav.setRoot(PayPremiumPage);
            }
          }
        ]
      });
        confirm.present();
    }
  }

  ionViewCanLeave(){
    console.log(this.myTracks);
  }

  ngAfterContentInit() {     
    this.allTracks = this._audioProvider.tracks; 
  }
  
  playSelectedTrack() {
    this._audioProvider.play(this.myTracks[0]);
  }
  
  pauseSelectedTrack() {
     this._audioProvider.pause(this.myTracks[0]);
  }
         
  onTrackFinished(track: any) {
    console.log('Track finished', track)
  }

  loadAudios(){
    let url = "http://secure.altamenteapp.com/api/general/meditations?auth-key="+this.login.token;
    this.http.get(url).subscribe(
      response=>{
        let data = response.json();
        for (let i=0;i<data.length;i++){
          let datos = data[i];
          this.myTracks.push({
            src: "http://secure.altamenteapp.com"+datos.url,
            title: datos.nombre,
            preload: 'true'
          });
        }
      },
      error => console.error(error)
    );
  } 
}
