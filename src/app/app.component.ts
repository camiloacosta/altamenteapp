import { Component, ViewChild } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar, Splashscreen, OneSignal} from 'ionic-native';
import { StartPage } from '../pages/start/start';
import { StreamingPage } from '../pages/streaming/streaming';
import { ContactoPage } from '../pages/contacto/contacto';
import {Login} from "../providers/login";
import { AlertController } from 'ionic-angular';
import {HomeLoginPage} from '../pages/home-login/home-login';
import { MeditacionesPage} from '../pages/meditaciones/meditaciones';
import { SessioncoachPage } from '../pages/sessioncoach/sessioncoach';
import { AsesoriaPage } from '../pages/asesoria/asesoria';
import { LibromesPage } from '../pages/libromes/libromes';
import { MusicPage } from '../pages/music/music';
import { CronogramaPage } from '../pages/cronograma/cronograma';
import { IntroduccionPage } from '../pages/introduccion/introduccion';
import { Message } from '../providers/message';
import {InboxPage} from '../pages/inbox/inbox';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  pageLogin: HomeLoginPage;
  profileData:{nombres:string,correo:string,foto:string} = {nombres:"",correo:"",foto:""};

  pages: Array<{title: string, component: any, icon:string}>;

  constructor(
    public platform: Platform,
    public login:Login,
    public alertCtrl: AlertController,
    public message:Message) {
    this.initializeApp();
    this.pages = [
      { title: 'Inicio', component: StartPage, icon:'home'},
      { title: 'Mensajes', component: InboxPage , icon:'paper-plane' },
      { title: 'Meditaciones', component: MeditacionesPage, icon:'bookmarks'},
      { title: 'Session privada de coaching', component: SessioncoachPage, icon:'chatbubbles'},
      { title: 'Asesoria empresarial', component: AsesoriaPage, icon:'briefcase'},
      { title: 'Libro del mes', component: LibromesPage, icon:'book'},
      { title: 'Musica consciente', component: MusicPage, icon:'musical-note'},
      { title: 'Cronograma anual', component: CronogramaPage, icon:'calendar'},
      { title: 'Charla introductoria', component: IntroduccionPage, icon:'play'},
      { title: 'Contacto', component: ContactoPage, icon:'contact'}
    ];
  }
  

  initializeApp() {
    this.platform.ready().then(() => {

      StatusBar.styleDefault();
      this.hideSplashScreen();
      if(this.login.isLogin()){
        this.rootPage = StartPage;
      }else{
        this.rootPage = HomeLoginPage;
      }
    });

    OneSignal.startInit('5cf8a39f-5324-491d-8091-928197c608c5','912336222276');
    OneSignal.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert);
    OneSignal.handleNotificationReceived().subscribe((response)=>{
      let data = response.data.notification.payload;
      this.message.save(data);
    });
    OneSignal.handleNotificationOpened().subscribe((response)=>{
      let data = response.notification.payload;
      this.message.save(data);
    });
    OneSignal.endInit();
  }

  hideSplashScreen(){
    if (Splashscreen) {
      setTimeout(() => {
      Splashscreen.hide();
      }, 100);
    }
  }

  Logout(){
    this.login.logout();
    this.nav.setRoot(HomeLoginPage);
  }

  isLogin():boolean{
    console.log(this.login.isLogin());
    return this.login.isLogin();
  }

  goToStreaming(data){
    this.nav.setRoot(StreamingPage,{room_id:data.additionalData.room_id});
  }

  openPage(page) {

    this.nav.setRoot(page.component);
  }

}
