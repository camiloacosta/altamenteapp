import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicAudioModule } from "ionic-audio";

//COMPONENTS --------------------------------------------------
import { HeadernavComponent} from '../components/headernav/headernav';
import {TabsComponent} from "../components/tabs/tabs";
import {PayComponent} from "../components/pay/pay";

//PROVIDERS ---------------------------------------------------
import { Login } from '../providers/login';
import { Register } from '../providers/register';
import { Message } from '../providers/message';
//PAGES -------------------------------------------------------
import {StartPage} from '../pages/start/start';
import {MessagesPage} from "../pages/messages/messages";
import {ConferencePage} from "../pages/conference/conference";
import {HomeLoginPage} from "../pages/home-login/home-login";
import {AudioPage} from '../pages/audio/audio';
import {CoursePage} from '../pages/course/course';
import {PhotogalleryPage} from '../pages/photogallery/photogallery';
import {AlbumPage} from '../pages/album/album';
import {RegisterPage} from '../pages/register/register';
import {EventsPage} from "../pages/events/events";
import {EditprofilePage} from "../pages/editprofile/editprofile";
import {PollsPage} from "../pages/polls/polls";
import {SurveysPage} from "../pages/surveys/surveys";
import { ProfilePage } from'../pages/profile/profile';
import { Home } from '../pages/home/home';
import { Maestrias } from '../pages/maestrias/maestrias';
import { ContactoPage } from '../pages/contacto/contacto';
import { LoginPage } from '../pages/login/login';
import { SuscribePage } from '../pages/suscribe/suscribe';
import { MeditacionesPage} from '../pages/meditaciones/meditaciones';
import { StreamingPage } from '../pages/streaming/streaming';
import { SessioncoachPage } from '../pages/sessioncoach/sessioncoach';
import { AsesoriaPage } from '../pages/asesoria/asesoria';
import { LibromesPage } from '../pages/libromes/libromes';
import { MusicPage } from '../pages/music/music';
import { CronogramaPage } from '../pages/cronograma/cronograma';
import { IntroduccionPage } from '../pages/introduccion/introduccion';
import {CertificacionesPage} from '../pages/certificaciones/certificaciones';
import {ConferenciasPage} from '../pages/conferencias/conferencias';
import {CapacitacionPage} from '../pages/capacitacion/capacitacion';
import {InboxPage} from '../pages/inbox/inbox';
import {PayPremiumPage} from '../pages/pay-premium/pay-premium';
@NgModule({
  declarations: [
    MyApp,
    Home,
    Maestrias,
    ContactoPage,
    LoginPage,
    SuscribePage,
    ProfilePage,
    HeadernavComponent,
    AudioPage,
    CoursePage,
    PhotogalleryPage,
    AlbumPage,
    RegisterPage,
    EventsPage,
    EditprofilePage,
    PollsPage,
    SurveysPage,
    TabsComponent,
    StartPage,
    MessagesPage,
    ConferencePage,
    HomeLoginPage,
    MeditacionesPage,
    StreamingPage,
    SessioncoachPage,
    AsesoriaPage,
    LibromesPage,
    MusicPage,
    CronogramaPage,
    IntroduccionPage,
    CertificacionesPage,
    ConferenciasPage,
    CapacitacionPage,
    PayComponent,
    InboxPage,
    PayPremiumPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    IonicAudioModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    Maestrias,
    ContactoPage,
    LoginPage,
    SuscribePage,
    ProfilePage,
    HeadernavComponent,
    AudioPage,
    CoursePage,
    PhotogalleryPage,
    AlbumPage,
    RegisterPage,
    EventsPage,
    EditprofilePage,
    PollsPage,
    SurveysPage,
    TabsComponent,
    StartPage,
    MessagesPage,
    ConferencePage,
    HomeLoginPage,
    MeditacionesPage,
    StreamingPage,
    SessioncoachPage,
    AsesoriaPage,
    LibromesPage,
    MusicPage,
    CronogramaPage,
    IntroduccionPage,
    CertificacionesPage,
    ConferenciasPage,
    CapacitacionPage,
    PayComponent,
    InboxPage,
    PayPremiumPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
    Login,
    Register,
    Message
  ]
})
export class AppModule {}
