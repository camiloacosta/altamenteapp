import { Injectable } from '@angular/core';
import { Http,Headers,Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class Message {

  constructor(public http: Http) {
    console.log('Hello Message Provider');
  }

  send(menssage:{nombre:string,asunto:string,correo:string,celular:number,mensaje:string}):Observable<boolean>{
    let url ="http://secure.altamenteapp.com/api/general/contact";
    let body = `Contacto[nombre]=${menssage.nombre}&Contacto[asunto]=${menssage.asunto}&Contacto[correo]=${menssage.correo}&Contacto[celular]=${menssage.celular}&Contacto[mensaje]=${menssage.mensaje}`;
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(url,body,{headers:headers}).map((response : Response)=>{
      let data = response.json();
      if(data.error == true){
        return true;
      }else{
        return false;
      }
    });
  }

  save(data){
    if(data.additionalData.type == "empoderamiento"){
      let messages:Array<{type:string,body:string}>=[];
      if(localStorage.getItem('messages')){
        let messagesLS = JSON.parse(localStorage.getItem('messages'));
        messages = messagesLS;
      }
      messages.push({
        type: data.additionalData.type,
        body:data.body
      });
      localStorage.setItem('messages',JSON.stringify(messages));
    }else{
      let notifications:Array<{type:string,body:string,room_id:string}>=[];
      if(localStorage.getItem('notifications')){
        let notificationsLS = JSON.parse(localStorage.getItem('notifications'));
        notifications = notificationsLS;
      }
      notifications.push({
          type:data.additionalData.type,
          body:data.body,
          room_id:data.additionalData.room_id
      });
      localStorage.setItem('notifications',JSON.stringify(notifications));
  
    }
 }

}
