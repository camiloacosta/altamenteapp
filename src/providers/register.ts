import { Injectable } from '@angular/core';
import {Http,Response,Headers} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Register provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Register {

  constructor(public http: Http) {
    
  }

  register(
    correo_electronico:string,
    nombres:string,
    apellidos:string,
    password:string,
    password_repeat:string,
    genero:string
    ){
      let url = "http://secure.altamenteapp.com/api/user/register";
      let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
      let body = `Usuarios[correo_electronico]=${correo_electronico}&Usuarios[nombres]=${nombres}&Usuarios[apellidos]=${apellidos}&Usuarios[password]=${password}&Usuarios[repeat_password]=${password_repeat}&Usuarios[genero]=${genero}`;
      return this.http.post(url,body,{headers : headers}).map((response : Response)=>{
        if(response.json().error){
          return false;
        }else{
          return true;
        }
      });
    }

}
