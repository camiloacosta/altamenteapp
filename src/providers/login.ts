import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import { Observable } from 'rxjs' 
import 'rxjs/add/operator/map';
@Injectable()
export class Login {
  public token: string;
  public pay:boolean;
  constructor(public http: Http) {
    var user = JSON.parse(localStorage.getItem('auth_key'));
    this.token = user && user.auth_key;
    var pago = JSON.parse(localStorage.getItem('pay'));
    this.pay = pago && pago.pay;
}

  login(user:string,pass:string):Observable<boolean>{
    let url = "http://secure.altamenteapp.com/api/user/login";
    let body = `Usuarios[email]=${user}&Usuarios[password]=${pass}`;
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    return this.http.post(url,body,{ headers : headers }).map((response : Response) => {
      let token = response.json() && response.json().auth_key;
      let pay = response.json() && response.json().pago;
      if (token){
        this.token = token;
        localStorage.setItem('auth_key', JSON.stringify({auth_key:this.token}));   
        this.pay = pay;
        localStorage.setItem('pay', JSON.stringify({pay:this.pay}));   
        return true;
      }else{
        return false;
      }
    });
  }

  logout():void{
    this.token = null;
    this.pay = null;
    localStorage.removeItem('auth_key');
    localStorage.removeItem('pay');
  }

  isLogin():boolean{
    let token = JSON.parse(localStorage.getItem('auth_key'));
    if(this.token && token.auth_key){
      return true;
    }else{
      return false;
    }
  }

}
